vim.loader.enable()
vim.api.nvim_command ":filetype on"
vim.api.nvim_command ":filetype plugin on"
vim.api.nvim_command ":filetype indent on"

-- 全局引导键
vim.api.nvim_set_keymap("", "<Space>", "<Nop>", { noremap = true, silent = true })
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

require "timly.core"
require "timly.lazy"
