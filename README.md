# 安装 neovim

## 安装字体 nerd-fond

```sh
sudo pacman -S noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
```

### 安装 ripgrep

```sh
sudo pacman -S ripgrep gd
```
