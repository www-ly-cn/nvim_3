-- local M = {
--   "lewis6991/gitsigns.nvim",
--   -- cmd = "Gitsigns",
--   event = { "BufReadPre", "BufNewFile" },
--   dependencies = {
--     {
--       -- 其实 lazygit 可以处理
--       "dinhhuy258/git.nvim", -- git 浏览历史提交记录
--       enabled = true,
--       event = { "BufReadPre", "BufNewFile" },
--       config = function()
--         require("git").setup {
--           -- Open blame window
--           blame = "<leader>gb",
--           -- Open file/folder in git repostitory(远程仓库)
--           browse = "<leader>go",
--         }
--       end,
--     },
--     {
--       "kdheepak/lazygit.nvim",
--       enabled = true,
--       cmd = {
--         "LazyGit",
--         "LazyGitConfig",
--         "LazyGitCurrentFile",
--         "LazyGitFilter",
--         "LazyGitFilterCurrentFile",
--       },
--       -- optional for floating window border decoration
--       dependencies = {
--         "nvim-lua/plenary.nvim",
--       },
--       keys = {
--         {
--           "<leader>gt",
--           "<cmd>LazyGit<CR>",
--           desc = "Open lazygit",
--         },
--       },
--     },
--   },
-- }
-- M.config = function()
--   -- require("gitsigns").setup {
--   --   signs = {
--   --     add = { text = "+" },
--   --     change = { text = "~" },
--   --     delete = { text = "_" },
--   --     topdelete = { text = "‾" },
--   --     changedelete = { text = "~" },
--   --     untracked = { text = "┆" },
--   --   },
--   -- }
--
--   local status_ok, gitsigns = pcall(require, "gitsigns")
--   if not status_ok then
--     vim.notify "gitsigns not found!"
--     return
--   end
--
--   gitsigns.setup {
--     signs = {
--       add = {
--         text = "+",
--         -- hl = 'GitSignsAdd',
--         -- numhl = 'GitSignsAddNr',
--         -- linehl = 'GitSignsAddLn'
--       },
--       change = {
--         text = "│",
--         -- hl = 'GitSignsChange',
--         -- numhl = 'GitSignsChangeNr',
--         -- linehl = 'GitSignsChangeLn'
--       },
--       delete = {
--         text = "_",
--         -- hl = 'GitSignsDelete',
--         -- numhl = 'GitSignsDeleteNr',
--         -- linehl = 'GitSignsDeleteLn'
--       },
--       topdelete = {
--         text = "‾",
--         -- hl = 'GitSignsDelete',
--         -- numhl = 'GitSignsDeleteNr',
--         -- linehl = 'GitSignsDeleteLn'
--       },
--       changedelete = {
--         text = "~",
--         -- hl = 'GitSignsChange',
--         -- numhl = 'GitSignsChangeNr',
--         -- linehl = 'GitSignsChangeLn'
--       },
--       untracked = {
--         text = "┆",
--         -- hl = 'GitSignsUntracked',
--         -- numhl = 'GitSignsUntrackedNr',
--         -- linehl = 'GitSignsUntrackedLn'
--       },
--     },
--     signcolumn = true, -- Toggle with `:Gitsigns toggle_signs`
--     numhl = false, -- Toggle with `:Gitsigns toggle_numhl`
--     linehl = false, -- Toggle with `:Gitsigns toggle_linehl`
--     word_diff = false, -- Toggle with `:Gitsigns toggle_word_diff`
--     watch_gitdir = {
--       interval = 1000,
--       follow_files = true,
--     },
--     attach_to_untracked = true,
--     current_line_blame = true, -- Toggle with `:Gitsigns toggle_current_line_blame`
--     current_line_blame_opts = {
--       virt_text = true,
--       virt_text_pos = "eol", -- 'eol' | 'overlay' | 'right_align'
--       delay = 100,
--       ignore_whitespace = false,
--     },
--     -- current_line_blame_formatter_opts = {
--     --   relative_time = false,
--     -- },
--     sign_priority = 6,
--     update_debounce = 100,
--     status_formatter = nil, -- Use default
--     max_file_length = 40000,
--     preview_config = {
--       -- Options passed to nvim_open_win
--       border = "single",
--       style = "minimal",
--       relative = "cursor",
--       row = 0,
--       col = 1,
--     },
--     -- yadm = {
--     --   enable = false,
--     -- },
--     -- keymapping
--     on_attach = function(bufnr)
--       local map = function(mode, lhs, rhs, opts)
--         opts = vim.tbl_extend("force", { noremap = true, silent = true }, opts or {})
--         vim.api.nvim_buf_set_keymap(bufnr, mode, lhs, rhs, opts)
--       end
--
--       -- Navigation
--       map("n", "<leader>j", ":Gitsigns next_hunk<CR>", { desc = "Next hunk" })
--       map("n", "<leader>k", ":Gitsigns prev_hunk<CR>", { desc = "Previous hunk" })
--
--       -- Actions
--       map("n", "<leader>hs", ":Gitsigns stage_hunk<CR>", { desc = "Stage hunk" })
--       map("n", "<leader>hr", ":Gitsigns reset_hunk<CR>", { desc = "Reset hunk" })
--       map("n", "<leader>hu", "<cmd>Gitsigns undo_stage_hunk<CR>", { desc = "undo stage hunk" })
--       map("n", "<leader>hS", "<cmd>Gitsigns stage_buffer<CR>", { desc = "stage buffer" })
--       map("n", "<leader>hR", "<cmd>Gitsigns reset_buffer<CR>", { desc = "reset_buffer" })
--       map("n", "<leader>hp", "<cmd>Gitsigns preview_hunk<CR>", { desc = "preview hunk" })
--       map(
--         "n",
--         "<leader>hb",
--         '<cmd>lua require"gitsigns".blame_line{full=true}<CR>',
--         { desc = "blame line" }
--       )
--       map(
--         "n",
--         "<leader>tb",
--         "<cmd>Gitsigns toggle_current_line_blame<CR>",
--         { desc = "toggle current line blame" }
--       )
--       map("n", "<leader>hd", "<cmd>Gitsigns diffthis<CR>", { desc = "diff this git" })
--       map("n", "<leader>hD", '<cmd>lua require"gitsigns".diffthis("~")<CR>', { desc = "diff " })
--       map("n", "<leader>td", "<cmd>Gitsigns toggle_deleted<CR>", { desc = "deleted" })
--       -- Text object
--       map("o", "ih", ":<C-U>Gitsigns select_hunk<CR>", { desc = "select hun (o)" })
--       map("x", "ih", ":<C-U>Gitsigns select_hunk<CR>", { desc = "select hunk (x)" })
--     end,
--   }
-- end
--
-- return M

return {
  {
    "lewis6991/gitsigns.nvim",
    event = { "BufReadPre", "BufNewFile" },
    opts = {
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, desc)
          vim.keymap.set(mode, l, r, { buffer = bufnr, desc = desc })
        end

        -- Navigation
        map("n", "]h", gs.next_hunk, "Next Hunk")
        map("n", "[h", gs.prev_hunk, "Prev Hunk")

        -- Actions
        map("n", "<leader>hs", gs.stage_hunk, "Stage hunk")
        map("n", "<leader>hr", gs.reset_hunk, "Reset hunk")
        map("v", "<leader>hs", function()
          gs.stage_hunk { vim.fn.line ".", vim.fn.line "v" }
        end, "Stage hunk")
        map("v", "<leader>hr", function()
          gs.reset_hunk { vim.fn.line ".", vim.fn.line "v" }
        end, "Reset hunk")

        map("n", "<leader>hS", gs.stage_buffer, "Stage buffer")
        map("n", "<leader>hR", gs.reset_buffer, "Reset buffer")

        map("n", "<leader>hu", gs.undo_stage_hunk, "Undo stage hunk")

        map("n", "<leader>hp", gs.preview_hunk, "Preview hunk")

        map("n", "<leader>hb", function()
          gs.blame_line { full = true }
        end, "Blame line")
        map("n", "<leader>hB", gs.toggle_current_line_blame, "Toggle line blame")

        map("n", "<leader>hd", gs.diffthis, "Diff this")
        map("n", "<leader>hD", function()
          gs.diffthis "~"
        end, "Diff this ~")

        -- Text object
        map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>", "Gitsigns select hunk")
      end,
    },
  },
  {
    -- 其实 lazygit 可以处理
    "dinhhuy258/git.nvim", -- git 浏览历史提交记录
    enabled = true,
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      require("git").setup {
        -- Open blame window
        blame = "<leader>gb",
        -- Open file/folder in git repostitory(远程仓库)
        browse = "<leader>go",
      }
    end,
  },
  {
    "kdheepak/lazygit.nvim",
    enabled = true,
    cmd = {
      "LazyGit",
      "LazyGitConfig",
      "LazyGitCurrentFile",
      "LazyGitFilter",
      "LazyGitFilterCurrentFile",
    },
    -- optional for floating window border decoration
    dependencies = {
      "nvim-lua/plenary.nvim",
    },
    keys = {
      {
        "<leader>gt",
        "<cmd>LazyGit<CR>",
        desc = "Open lazygit",
      },
    },
  },
}
