--[[ local M = {
  "akinsho/bufferline.nvim",
  enabled = true,
  version = "*",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    "famiu/bufdelete.nvim",
  },
  opts = {
    options = {
      mode = "tabs",
      separator_style = "slant",
    },
  },
}

return M ]]

local M = {
  "akinsho/bufferline.nvim",
  enabled = true,
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    "famiu/bufdelete.nvim",
  },
}

function M.config()
  local status, bufferline = pcall(require, "bufferline")
  if not status then
    vim.notify "Not found 'bufferline.nvim' plugin"
    return
  end

  -- gt 切换到下一个，gT切换到上一个
  vim.keymap.set("n", "gt", "<cmd>BufferLineCycleNext<CR>")
  vim.keymap.set("n", "gT", "<cmd>BufferLineCyclePrev<CR>")

  -- 根据标题给出的字母提示操作，按下对应的就关闭对应的tab bufferline
  vim.keymap.set("n", "<leader>bq", "<cmd>BufferLinePickClose<CR>")

  vim.keymap.set("n", "<leader>bn", "<cmd>BufferLineMoveNext<cr>")
  vim.keymap.set("n", "<leader>bp", "<cmd>BufferLineMovePrev<cr>")
  -- vim.keymap.set("n", "<leader>bcl", "<cmd>BufferLineCloseRight<cr>")
  -- vim.keymap.set("n", "<leader>bch", "<cmd>BufferLineCloseLeft<cr>")
  -- vim.keymap.set("n", "<leader>bco", "<cmd>BufferLineCloseOthers<cr>")
  -- vim.keymap.set("n", "<leader>bc", "<cmd>BufferLineCloseOthers<cr><cmd>Bdelete<cr>")
  -- 关闭当前
  vim.keymap.set("n", "<leader>bd", "<cmd>Bdelete<cr>")
  -- 关闭所有
  vim.keymap.set("n", "<leader>ba", "<cmd>BufferLineCloseOthers<cr><cmd>Bdelete<cr>")
  -- 关闭除当前buffer外的所有buffer
  vim.keymap.set("n", "<leader>bc", "<cmd>BufferLineCloseOthers<cr>")
  vim.keymap.set("n", "<leader>bo", "<cmd>BufferLineCloseLeft<CR><cmd>BufferLineCloseRight<CR>")

  -- bufferline config
  -- https://github.com/akinsho/bufferline.nvim#configuration
  bufferline.setup {
    options = {
      -- 模式改为 tabs ，只显示真实的tabs不显示buffer
      -- mode = "tabs",
      mode = "buffers",
      -- 显示id
      numbers = "ordinal",
      -- 左侧让出 nvim-tree 的位置
      offsets = {
        {
          filetype = "NvimTree",
          text = "File Explorer",
          highlight = "Directory",
          text_align = "left",
        },
      },
      -- To close the Tab command, use moll/vim-bbye's :Bdelete command here
      close_command = "Bdelete! %d",
      right_mouse_command = "Bdelete! %d",
      -- 使用 cmp-nvim-lsp
      diagnostics = "nvim_lsp",
      -- 标题同时显示错误和警告 需要 lsp 中的支持
      -- Optional, show LSP error icon
      ---@diagnostic disable-next-line: unused-local
      diagnostics_indicator = function(count, level, diagnostics_dict, context)
        local s = " "
        for e, n in pairs(diagnostics_dict) do
          local sym = e == "error" and " " or (e == "warning" and " " or "󰌵 ") -- " " " " " " "󰌵 "
          s = s .. sym .. n .. " "
        end
        return s
      end,
      -- 动态显示关闭键
      hover = {
        enabled = false,
        delay = 200,
        reveal = { "close" },
      },
    },
  }
end

return M
