--[[ local M = {
  "hrsh7th/nvim-cmp",
  event = "InsertEnter",
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-nvim-lsp-document-symbol",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "hrsh7th/cmp-emoji",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-nvim-lua",
    "FelipeLema/cmp-async-path", -- { name = 'async_path' }
    "saadparwaiz1/cmp_luasnip",
    "lukas-reineke/cmp-rg", -- { name = "rg" }
    {
      "L3MON4D3/LuaSnip",
      build = (function()
        if vim.fn.has "win32" == 1 or vim.fn.executable "make" == 9 then
          return
        end
        return "make install_jsregexp"
      end)(),
      dependencies = {
        "rafamadriz/friendly-snippets",
      },
    },
    "onsails/lspkind.nvim",
  },
}

function M.config()
  local cmp = require "cmp"
  local luasnip = require "luasnip"
  local lspkind = require "lspkind"

  vim.api.nvim_set_hl(0, "CmpItemKindEmoji", { fg = "#FDE030" })
  require("luasnip.loaders.from_vscode").lazy_load()

  cmp.setup({
    completion = {
      completeopt = "menu,menuone,preview,noselect",
    },
    snippet = { -- configure how nvim-cmp interacts with snippet engine
      expand = function(args)
        luasnip.lsp_expand(args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert({
      ["<C-k>"] = cmp.mapping.select_prev_item(), -- previous suggestion
      ["<C-j>"] = cmp.mapping.select_next_item(), -- next suggestion
      ["<C-b>"] = cmp.mapping.scroll_docs(-4),
      ["<C-f>"] = cmp.mapping.scroll_docs(4),
      ["<C-Space>"] = cmp.mapping.complete(), -- show completion suggestions
      ["<C-e>"] = cmp.mapping.abort(), -- close completion window
      ["<CR>"] = cmp.mapping.confirm({ select = false }),
    }),
    -- sources for autocompletion
    sources = cmp.config.sources({
      { name = "nvim_lsp" },
      { name = "luasnip" }, -- snippets
      { name = "buffer" }, -- text within current buffer
      { name = "path" }, -- file system paths
    }),
    sources = {
      { name = "nvim_lsp", keyword_length = 2 }, -- lsp cmp-nvim-lsp
      { name = "buffer", keyword_length = 2 },
      { name = "path", keyword_length = 2 }, -- file system paths
      { name = "nvim_lsp_document_symbol", keyword_length = 3 }, -- document-symbol
      { name = "nvim_lsp_signature_help", keyword_length = 3 }, -- signature-help
      { name = "emoji" },
      { name = "async_path", keyword_length = 2 }, -- file system paths (async)
      { name = "luasnip", keyword_length = 2 }, -- luasnip snippets
      { name = "rg", keyword_length = 3 }, -- rg serach
      { name = "nvim_lua" },
    },
    -- configure lspkind for vs-code like pictograms in completion menu
    formatting = {
      format = lspkind.cmp_format({
        maxwidth = 50,
        ellipsis_char = "...",
      }),
    },
  })

  -- 根据文件类型来选择补全来源
  cmp.setup.filetype("gitcommit", {
    sources = cmp.config.sources {
      { name = "git" },
      { name = "buffer" },
    },
  })
  -- 命令模式下输入 `/` 、`?`启用补全
  cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline {
      -- 覆盖默认的快捷键
      ["<CR>"] = {
        c = cmp.mapping.confirm { select = false },
      },
    },
    sources = {
      { name = "buffer" },
    },
  })
  -- 命令模式下输入 `:` 启用补全
  cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline {
      -- 覆盖默认的快捷键
      ["<CR>"] = {
        c = cmp.mapping.confirm { select = false },
      },
    },
    sources = cmp.config.sources {
      { name = "path" },
      { name = "cmdline" },
    },
  })
end

return M ]]

local M = {
  -- nvim-cmp
  -- cmp-nvim-lsp
  -- cmp-buffer
  -- cmp-path
  -- cmp_luasnip
  -- LuaSnip
  -- friendly-snippets
  "hrsh7th/nvim-cmp",
  dependencies = {
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-emoji",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-nvim-lua",
    "hrsh7th/cmp-calc",
    "hrsh7th/cmp-nvim-lsp-document-symbol",
    "hrsh7th/cmp-nvim-lsp-signature-help",
    "FelipeLema/cmp-async-path", -- { name = 'async_path' }
    "saadparwaiz1/cmp_luasnip",
    {
      "L3MON4D3/LuaSnip",
      build = (function()
        if vim.fn.has "win32" == 1 or vim.fn.executable "make" == 9 then
          return
        end
        return "make install_jsregexp"
      end)(),
      dependencies = {
        "rafamadriz/friendly-snippets",
      },
    },
    "lukas-reineke/cmp-rg", -- { name = "rg" }
    "f3fora/cmp-spell",
    "ray-x/cmp-treesitter", -- { name = 'treesitter' },
    {
      "David-Kunz/cmp-npm", -- {name = "npm"}
      dependencies = { "nvim-lua/plenary.nvim" },
      ft = "json",
      config = function()
        require("cmp-npm").setup {}
      end,
    },

    -- icon
    "onsails/lspkind.nvim",
  },
}

function M.config()
  vim.opt.completeopt = { "menu", "menuone", "noselect" }

  require("luasnip.loaders.from_vscode").lazy_load()

  local cmp = require "cmp"
  local luasnip = require "luasnip"
  local lspkind = require "lspkind"

  local select_opts = { behavior = cmp.SelectBehavior.Select }

  vim.api.nvim_set_hl(0, "CmpItemKindEmoji", { fg = "#FDE030" })

  lspkind.init {
    mode = "symbol_text",
    preset = "codicons",
    symbol_map = {
      Text = "󰉿",
      Method = "󰆧",
      Function = "󰊕",
      Constructor = "",
      Field = "󰜢",
      Variable = "󰀫",
      Class = "󰠱",
      Interface = "",
      Module = "",
      Property = "󰜢",
      Unit = "󰑭",
      Value = "󰎠",
      Enum = "",
      Keyword = "󰌋",
      Snippet = "",
      Color = "󰏘",
      File = "󰈙",
      Reference = "󰈇",
      Folder = "󰉋",
      EnumMember = "",
      Constant = "󰏿",
      Struct = "󰙅",
      Event = "",
      Operator = "󰆕",
      TypeParameter = " ",
    },
  }

  cmp.setup {
    snippet = {
      expand = function(args)
        luasnip.lsp_expand(args.body)
      end,
    },
    sources = {
      { name = "nvim_lsp", keyword_length = 2 }, -- lsp cmp-nvim-lsp
      { name = "buffer", keyword_length = 2 },
      -- {
      --   -- text within current buffer
      --   name = "buffer",
      --   option = {
      --     get_bufnrs = function()
      --       return vim.api.nvim_list_bufs()
      --     end,
      --   },
      -- },
      { name = "luasnip", keyword_length = 2 }, -- luasnip snippets
      { name = "path", keyword_length = 2 }, -- file system paths
      { name = "async_path", keyword_length = 2 }, -- file system paths (async)
      { name = "rg", keyword_length = 3 }, -- rg serach
      { name = "nvim_lsp_document_symbol", keyword_length = 3 }, -- document-symbol
      { name = "nvim_lsp_signature_help", keyword_length = 3 }, -- signature-help
      { name = "nvim_lua" },
      {
        -- vim.opt.spell = true
        -- vim.opt.spelllang = { 'en_us', 'cjk' }
        name = "spell", -- cmp-spell
        option = {
          keep_all_entries = true,
          enable_in_context = function()
            return require("cmp.config.context").in_treesitter_capture "spell"
          end,
        },
        keyword_length = 4,
      },
      { name = "calc" },
      { name = "treesitter" }, -- cmp-treesitter
      { name = "npm", keyword_length = 2 }, -- package.json  npm
      { name = "crates" }, -- rust -> crates
      { name = "emoji" },
      { name = "copilot" }, -- "zbirenbaum/copilot-cmp",
    },
    window = {
      documentation = cmp.config.window.bordered(),
    },
    experimental = {
      -- 编辑器像有一个灰色倒影的预测用户输入书写提示
      ghost_text = true,
      native_menu = false,
    },
    formatting = {
      -- fields = { 'menu', 'abbr', 'kind' },
      fields = { "abbr", "kind", "menu" },
      -- Source 显示提示来源 lsp, snippets, path 等等
      format = function(entry, item)
        -- local menu_icon = {
        -- 	nvim_lsp = 'λ',
        -- 	luasnip = '⋗',
        -- 	buffer = 'Ω',
        -- 	path = '🖫',
        -- }
        -- item.menu = menu_icon[entry.source.name]
        item.menu = "[" .. string.upper(entry.source.name) .. "]"

        -- return item

        if vim.tbl_contains({ "path" }, entry.source.name) then
          local icon, hl_group = require("nvim-web-devicons").get_icon(entry:get_completion_item().label)
          if icon then
            item.kind = icon
            item.kind_hl_group = hl_group
            return item
          end
        end

        if entry.source.name == "crates" then
          item.kind = " "
          item.kind_hl_group = "CmpItemKindCrate"
        end

        if entry.source.name == "emoji" then
          item.kind = ""
          item.kind_hl_group = "CmpItemKindEmoji"
        end

        return require("lspkind").cmp_format { with_text = true }(entry, item)
      end,
    },
    mapping = {
      ["<C-k>"] = cmp.mapping.select_prev_item(), -- previous suggestion
      ["<C-j>"] = cmp.mapping.select_next_item(), -- next suggestion
      ["<Up>"] = cmp.mapping.select_prev_item(select_opts),
      ["<Down>"] = cmp.mapping.select_next_item(select_opts),
      ["<C-p>"] = cmp.mapping.select_prev_item(select_opts),
      ["<C-n>"] = cmp.mapping.select_next_item(select_opts),

      -- ["<C-b>"] = cmp.mapping.scroll_docs(-4),
      -- ["<C-f>"] = cmp.mapping.scroll_docs(4),
      ["<C-u>"] = cmp.mapping.scroll_docs(-4),
      ["<C-d>"] = cmp.mapping.scroll_docs(4),

      ["<C-Space>"] = cmp.mapping.complete(), -- show completion suggestions
      ["<C-e>"] = cmp.mapping.abort(), -- close completion window
      ["<C-y>"] = cmp.mapping.confirm { select = true },
      ["<CR>"] = cmp.mapping.confirm { select = false },

      ["<C-f>"] = cmp.mapping(function(fallback)
        if luasnip.jumpable(1) then
          luasnip.jump(1)
        else
          fallback()
        end
      end, { "i", "s" }),
      ["<C-b>"] = cmp.mapping(function(fallback)
        if luasnip.jumpable(-1) then
          luasnip.jump(-1)
        else
          fallback()
        end
      end, { "i", "s" }),

      ["<Tab>"] = cmp.mapping(function(fallback)
        local col = vim.fn.col "." - 1

        if cmp.visible() then
          cmp.select_next_item(select_opts)
        elseif col == 0 or vim.fn.getline("."):sub(col, col):match "%s" then
          fallback()
        else
          cmp.complete()
        end
      end, { "i", "s" }),
      ["<S-Tab>"] = cmp.mapping(function(fallback)
        if cmp.visible() then
          cmp.select_prev_item(select_opts)
        else
          fallback()
        end
      end, { "i", "s" }),
    },
  }

  -- 根据文件类型来选择补全来源
  cmp.setup.filetype("gitcommit", {
    sources = cmp.config.sources {
      { name = "git" },
      { name = "buffer" },
    },
  })
  -- 命令模式下输入 `/` 、`?`启用补全
  cmp.setup.cmdline({ "/", "?" }, {
    mapping = cmp.mapping.preset.cmdline {
      -- 覆盖默认的快捷键
      ["<CR>"] = {
        c = cmp.mapping.confirm { select = false },
      },
    },
    sources = {
      { name = "buffer" },
    },
  })
  -- 命令模式下输入 `:` 启用补全
  cmp.setup.cmdline(":", {
    mapping = cmp.mapping.preset.cmdline {
      -- 覆盖默认的快捷键
      ["<CR>"] = {
        c = cmp.mapping.confirm { select = false },
      },
    },
    sources = cmp.config.sources {
      { name = "path" },
      { name = "cmdline" },
    },
  })
end

return M
