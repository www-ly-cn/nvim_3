local M = {
  -- 启动界面
  "goolord/alpha-nvim",
  -- http://yyq123.github.io/learn-vim/learn-vi-49-01-autocmd.html
  event = "VimEnter", -- GUIEnter VimEnter
  dependencies = {
    "nvim-tree/nvim-web-devicons",
  },
}

function M.config()
  local alpha = require "alpha"
  local dashboard = require "alpha.themes.dashboard"

  local function button(sc, txt, keybind, keybind_opts)
    local b = dashboard.button(sc, txt, keybind, keybind_opts)
    b.opts.hl_shortcut = "Include"
    return b
  end

  dashboard.section.header.val = {
    "                                                     ",
    "  ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗ ",
    "  ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║ ",
    "  ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║ ",
    "  ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║ ",
    "  ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║ ",
    "  ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝ ",
    "                                                     ",
    [[     ★　 ✯   🛸  🛰      |      🛰   🛸  ✯  ★      ]],
    [[     ★　 ✯   🛸  🛰      |      🛰   🛸  ✯  ★      ]],
    [[     ★　 ✯   🛸  🛰      |      🛰   🛸  ✯  ★      ]],
    [[                        / \                        ]],
    [[                       | O |                       ]],
    [[                       | O |                       ]],
    [[                      /| | |\                      ]],
    [[                     /_(.|.)_\                     ]],
    "                                                     ",
    "            ⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣤⣴⣶⣶⣶⣶⣶⠶⣶⣤⣤⣀⠀⠀ ⠀⠀  ⠀ ⠀         ",
    "  ⠀⠀⠀        ⠀⠀⠀⠀⢀⣤⣾⣿⣿⣿⠁⠀⢀⠈⢿⢀⣀⠀⠹⣿⣿⣿⣦⣄⠀  ⠀  ⠀         ",
    "  ⠀       ⠀ ⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⠿⠀⠀⣟⡇⢘⣾⣽⠀⠀⡏⠉⠙⢛⣿⣷⡖⠀             ",
    "   ⠀⠀        ⠀⠀⣾⣿⣿⡿⠿⠷⠶⠤⠙⠒⠀⠒⢻⣿⣿⡷⠋⠀⠴⠞⠋⠁⢙⣿⣄             ",
    "   ⠀⠀        ⠀⢸⣿⣿⣯⣤⣤⣤⣤⣤⡄⠀⠀⠀⠀⠉⢹⡄⠀⠀⠀⠛⠛⠋⠉⠹⡇             ",
    "  ⠀⠀⠀        ⠀⢸⣿⣿⠀⠀⠀⣀⣠⣤⣤⣤⣤⣤⣤⣤⣼⣇⣀⣀⣀⣛⣛⣒⣲⢾⡷             ",
    "          ⢀⠤⠒⠒⢼⣿⣿⠶⠞⢻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠁⠀⣼⠃             ",
    "          ⢮⠀⠀⠀⠀⣿⣿⣆⠀⠀⠻⣿⡿⠛⠉⠉⠁⠀⠉⠉⠛⠿⣿⣿⠟⠁⠀⣼⠃⠀             ",
    "          ⠈⠓⠶⣶⣾⣿⣿⣿⣧⡀⠀⠈⠒⢤⣀⣀⡀⠀⠀⣀⣀⡠⠚⠁⠀⢀⡼⠃⠀ ⠀            ",
    "            ⠀⠈⢿⣿⣿⣿⣿⣿⣷⣤⣤⣤⣤⣭⣭⣭⣭⣭⣥⣤⣤⣤⣴⣟⠁                ",
  }

  local icons = {
    ui = {
      NewFile = " ",
      Files = "󰱽 ",
      SidebarCollapse = " ",
      Text = " ",
      SignIn = " ",
      SignOut = " ",
      Gear = " ",
      History = " ",
      SessionRestore = "󰁯 ",
    },
  }

  -- Set menu
  dashboard.section.buttons.val = {
    button("n", icons.ui.NewFile .. " New file", "<cmd>ene <BAR> startinsert<CR>"),
    button("SPC ff", icons.ui.Files .. " Find file", "<cmd>Telescope find_files<CR>"),
    button("SPC fs", icons.ui.Text .. " Find Word", "<cmd>Telescope live_grep<CR>"),
    button("SPC fr", icons.ui.History .. " Recent files", "<cmd>Telescope oldfiles<CR>"),
    button(
      "SPC pl",
      icons.ui.SessionRestore .. " Restore session for current Directore",
      '<cmd>lua require("persistence").load({ last = true })<cr>>'
    ),
    button("SPC ee", icons.ui.SidebarCollapse .. " Toggle file explorer", "<cmd>NvimTreeToggle<CR>"),
    button("c", icons.ui.Gear .. " Config", "<cmd>e ~/.config/nvim/init.lua<CR>"),
    button("q", icons.ui.SignOut .. " Quit", "<cmd>qa<CR>"),
  }

  -- Send config to alpha
  alpha.setup(dashboard.opts)

  -- Disable folding on alpha buffer
  vim.cmd [[autocmd FileType alpha setlocal nofoldenable]]
end

return M
