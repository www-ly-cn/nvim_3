return {
  "szw/vim-maximizer",
  keys = {
    { "<leader>sm", "<cmd>MaximizerToggle<cr>", desc = "Maximizer/minimize a split" },
  }
}
