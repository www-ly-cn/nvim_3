-- https://yelog.org/2024/08/02/write-markdown-in-neovim-experience-and-tips/
-- https://blog.csdn.net/weixin_44335269/article/details/117753679
return {
  {
    -- 终端样式实时渲染
    "yelog/marklive.nvim",
    enabled = false,
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    lazy = true,
    ft = "markdown",
    opts = {},
  },
  {
    -- 终端样式渲染
    -- https://www.dongaigc.com/p/OXY2DEV/markview.nvim
    "OXY2DEV/markview.nvim",
    lazy = false, -- 推荐
    enabled = false, -- 启用有bug, 会导致无法输入,大写 M
    ft = "markdown", -- 如果你决定延迟加载
    dependencies = {
      -- 如果你手动安装了解析器，就不需要这个
      -- 或者如果解析器在你的 $RUNTIMEPATH 中
      "nvim-treesitter/nvim-treesitter",
      "nvim-tree/nvim-web-devicons",
    },
    config = function()
      -- require("markview").setup {
      --   modes = { "n", "no", "c" }, -- 根据需要更改这些模式
      --
      --   hybrid_modes = { "n" }, -- 在普通模式下使用此功能
      --
      --   -- 这个设置很好
      --   callbacks = {
      --     on_enable = function(_, win)
      --       vim.wo[win].conceallevel = 2
      --       vim.wo[win].conecalcursor = "c"
      --     end,
      --   },
      -- }

      local markview = require "markview"
      -- local presets = require "markview.presets"
      markview.setup {
        -- headings = presets.headings.glow_labels,
        headings = {
          enable = true,

          heading_1 = {
            style = "label",

            padding_left = " ",
            padding_right = " ",

            hl = "MarkviewHeading1",
          },
        },
      }
      vim.cmd "Markview enableAll"

      -- require("markview").setup {
      --   modes = { "n", "no", "c" }, -- "i"
      --   hybrid_modes = { "i" },
      --   -- 这是一个不错的功能
      --   callbacks = {
      --     on_enable = function(_, win)
      --       vim.wo[win].conceallevel = 2
      --       vim.wo[win].concealcursor = "nc"
      --     end,
      --   },
      -- }
      vim.keymap.set({ "n", "i" }, "<S-m>", "<cmd>Markview splitToggle<cr>")
    end,
  },
  {
    -- 终端样式实时渲染
    "gaoDean/autolist.nvim",
    lazy = true,
    enabled = true,
    ft = { "markdown", "text", "tex", "plaintex", "norg" },
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    config = function()
      require("autolist").setup()

      vim.keymap.set("i", "<tab>", "<cmd>AutolistTab<cr>")
      vim.keymap.set("i", "<s-tab>", "<cmd>AutolistShiftTab<cr>")
      -- vim.keymap.set("i", "<c-t>", "<c-t><cmd>AutolistRecalculate<cr>") -- an example of using <c-t> to indent
      vim.keymap.set("i", "<CR>", "<CR><cmd>AutolistNewBullet<cr>")
      vim.keymap.set("n", "o", "o<cmd>AutolistNewBullet<cr>")
      vim.keymap.set("n", "O", "O<cmd>AutolistNewBulletBefore<cr>")
      vim.keymap.set("n", "<CR>", "<cmd>AutolistToggleCheckbox<cr><CR>")
      vim.keymap.set("n", "<C-r>", "<cmd>AutolistRecalculate<cr>")

      -- cycle list types with dot-repeat
      vim.keymap.set("n", "<leader>cn", require("autolist").cycle_next_dr, { expr = true })
      vim.keymap.set("n", "<leader>cp", require("autolist").cycle_prev_dr, { expr = true })

      -- if you don't want dot-repeat
      -- vim.keymap.set("n", "<leader>cn", "<cmd>AutolistCycleNext<cr>")
      -- vim.keymap.set("n", "<leader>cp", "<cmd>AutolistCycleNext<cr>")

      -- functions to recalculate list on edit
      vim.keymap.set("n", ">>", ">><cmd>AutolistRecalculate<cr>")
      vim.keymap.set("n", "<<", "<<<cmd>AutolistRecalculate<cr>")
      vim.keymap.set("n", "dd", "dd<cmd>AutolistRecalculate<cr>")
      vim.keymap.set("v", "d", "d<cmd>AutolistRecalculate<cr>")
    end,
  },

  {
    -- 浏览器实时预览
    "iamcco/markdown-preview.nvim",
    enabled = true,
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = function()
      vim.fn["mkdp#util#install"]()
      -- vim.g.mkdp_theme = "light"
      vim.g.mkdp_theme = "dark"
    end,
  },
  {
    "iamcco/markdown-preview.nvim",
    enabled = true,
    cmd = { "MarkdownPreviewToggle", "MarkdownPreview", "MarkdownPreviewStop" },
    ft = { "markdown" },
    build = "cd app && yarn install",
    init = function()
      vim.g.mkdp_filetypes = { "markdown" }
      vim.keymap.set(
        { "n", "i" },
        "<A-space>m",
        "<cmd>MarkdownPreviewToggle<cr>",
        { desc = "Markdown preview in browser" }
      )
    end,
  },

  {
    -- 普通列,支持有序列表, 无序列表, 任务列表等等, 可以缩进, 新增
    "bullets-vim/bullets.vim",
    enabled = true,
    config = function()
      vim.g.bullets_enabled_file_types = { "markdown", "text", "gitcommit", "scratch" }
    end,
  },
  {
    -- 任务状态切换插件
    "tenxsoydev/vim-markdown-checkswitch",
    enabled = true,
    -- 通过 :CheckSwitch 命令切换任务状态
    config = function()
      vim.g.md_checkswitch_style = "cycle"
    end,
  },
  {
    -- 表格, 支持在 vim/neovim 中编辑 markdown 表格的插件, 支持高亮, 自动对齐, 自动生成等
    "dhruvasagar/vim-table-mode",
    enabled = true,
    config = function()
      vim.cmd(
        [[
        augroup markdown_config
          autocmd!
          autocmd FileType markdown nnoremap <buffer> <M-s> :TableModeRealign<CR>
        augroup END
      ]],
        false
      )
      vim.g.table_mode_sort_map = "<leader>mts"
    end,
  },
}
