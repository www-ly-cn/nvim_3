--[[ local M = {
  "nvim-treesitter/nvim-treesitter",
  event = { "BufReadPre", "BufNewFile" },
  build = ":TSUpdate",
}

function M.config()
  local treesitter = require "nvim-treesitter.configs"

  local ensure_installed = {
    "json",
    "javascript",
    "typescript",
    "tsx",
    "yaml",
    "html",
    "css",
    "markdown",
    "markdown_inline",
    "svelte",
    "graphql",
    "bash",
    "lua",
    "vim",
    "dockerfile",
    "gitignore",
    "query",
    "vimdoc",
    "c",
    "go",
    "gomod",
    "gowork",
    "gosum",
    "rust",
  }

  treesitter.setup {
    -- ensure these language parsers are installed
    ensure_installed = ensure_installed,
    -- enable syntax highlighting
    highlight = { enable = true },
    -- enable indentation
    indent = { enable = true },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "<C-space>",
        node_incremental = "<C-space>",
        scope_incremental = false,
        node_decremental = "<bs>",
      },
    },
  }
end

return M ]]

local M = {
  "nvim-treesitter/nvim-treesitter",
  enabled = true,
  -- commit = "",
  event = { "BufReadPost", "BufNewFile" },
  build = ":TSUpdate",
  dependencies = {
    -- 增强 nvim-treesitter 的参数高亮
    "m-demare/hlargs.nvim",
    -- 更好的快捷操作， ciw caw vaw va{ da{
    "nvim-treesitter/nvim-treesitter-textobjects",
  },
}

function M.config()
  local status, treesitter = pcall(require, "nvim-treesitter.configs")
  if not status then
    vim.notify "Not found 'nvim-treesitter' plugin"
    return
  end

  local ensure_installed = {
    "rust",
    "toml",
    "json",
    "markdown",
    "markdown_inline",
    "lua",
    "http",
    "regex",
    "bash",
    "vim",
    "vimdoc",
    "yaml",
    "javascript",
    "typescript",
    "tsx",
    "html",
    "css",
    "scss",
    "vue",
    "python",
    "dockerfile",
    "go",
    "gomod",
    "c",
    "cmake",
    "make",
    "cpp",
    "comment",
    "diff",
    "git_config",
    "git_rebase",
    "gitattributes",
    "gitcommit",
    "gitignore",
    "meson",
    "sql",
    "zig",
    "v",
    "graphql",
    "norg", -- note tool
  }

  treesitter.setup {
    -- 安装 language parser
    -- :TSInstallInfo 命令查看支持的语言
    ensure_installed = ensure_installed,
    ignore_install = {},
    sync_install = false, -- 不同步安装
    auto_install = true,
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = true,
    },
    query_linter = {
      enable = true,
      use_virtual_text = true,
      lint_events = { "BufWrite", "CursorHold" },
    },
    -- nvim-treesitter 代码格式化
    -- map("n", "<leader>i", "gg=G", opt) -- 用 = 格式化代码
    indent = {
      enable = true,
      disable = { "yaml" },
    },
    -- 区域选择 -- 启用增量选择
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "<CR>", -- "gnn",
        node_incremental = "<CR>", -- "grn",
        scope_incremental = "<SPACE>", -- "grc",
        node_decremental = "<TAB>", -- "grm",
      },
    },

    -- 依赖插件 'nvim-treesitter/nvim-treesitter-textobjects'
    textobjects = {
      select = {
        enable = true,
        -- Automatically jump forward to textobj, similar to targets.vim
        lookahead = true,
        -- vif dif  f 函数
        -- diw viw  w 单词
        -- di" vi"  " 一些符号,例如, { ' " [
        keymaps = {
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",
          ["aa"] = "@parameter.outer",
          ["ia"] = "@parameter.inner",
          ["ac"] = "@class.outer",
          -- ['ic'] = '@class.inner',
          ["ic"] = { query = "@class.inner", desc = "Select inner part of a class region" },
          ["as"] = { query = "@scope", query_group = "locals", desc = "Select language scope" },
        },
        selection_modes = {
          ["@parameter.outer"] = "v", -- charwise
          ["@function.outer"] = "V", -- linewise
          ["@class.outer"] = "<c-v>", -- blockwise
        },
        include_surrounding_whitespace = false,
      },
      swap = {
        -- 调换参数
        enable = true,
        swap_next = {
          ["<A-space>n"] = "@parameter.inner",
        },
        swap_previous = {
          ["<A-space>A"] = "@parameter.inner",
        },
      },
      move = {
        enable = true,
        set_jumps = true, -- whether to set jumps in the jumplist
        goto_next_start = {
          ["]m"] = "@function.outer",
          ["]]"] = "@class.outer",
        },
        goto_next_end = {
          ["]M"] = "@function.outer",
          ["]["] = "@class.outer",
        },
        goto_previous_start = {
          ["[m"] = "@function.outer",
          ["[["] = "@class.outer",
        },
        goto_previous_end = {
          ["[M"] = "@function.outer",
          ["[]"] = "@class.outer",
        },
      },
    },
  }
end

return M
