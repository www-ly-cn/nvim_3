local M = {
  "sindrets/diffview.nvim",
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  cmd = { "DiffviewOpen", "DiffviewToggleFiles" },
}

M.config = function()
  require("diffview").setup {
    enhanced_diff_hl = true,
    hooks = {
      view_leave = function(_)
        vim.cmd [[DiffviewClose]]
      end,
      diff_buf_read = function()
        vim.wo.signcolumn = "no"
      end,
    },
  }
end

return M
