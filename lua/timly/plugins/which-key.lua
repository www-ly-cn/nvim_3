local M = {
  "folke/which-key.nvim",
  enabled = true,
  event = "VeryLazy",
  opts = {},
}

function M.init()
  vim.o.timeout = true
  vim.o.timeoutlen = 500
end

return M
