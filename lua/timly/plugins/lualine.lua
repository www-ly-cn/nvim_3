local M = {
  "nvim-lualine/lualine.nvim",
  dependencies = "nvim-tree/nvim-web-devicons",
}

function M.config()
  local lualine = require "lualine"
  local lazy_status = require "lazy.status"

  local colors = {
    blue = "#65d1ff",
    green = "#3effdc",
    violet = "#ff61ef",
    yellow = "#ffda7b",
    red = "#ff4a4a",
    fg = "#c3ccdc",
    bg = "#112638",
    inactive_bg = "#2c3043",
    semilightgray = "#ffffff",
  }

  local lualine_theme = {
    normal = {
      a = { bg = colors.blue, fg = colors.bg, gui = "bold" },
      b = { bg = colors.bg, fg = colors.fg },
      c = { bg = colors.bg, fg = colors.fg },
    },
    insert = {
      a = { bg = colors.green, fg = colors.bg, gui = "bold" },
      b = { bg = colors.bg, fg = colors.fg },
      c = { bg = colors.bg, fg = colors.fg },
    },
    visual = {
      a = { bg = colors.violet, fg = colors.bg, gui = "bold" },
      b = { bg = colors.bg, fg = colors.fg },
      c = { bg = colors.bg, fg = colors.fg },
    },
    command = {
      a = { bg = colors.yellow, fg = colors.bg, gui = "bold" },
      b = { bg = colors.bg, fg = colors.fg },
      c = { bg = colors.bg, fg = colors.fg },
    },
    replace = {
      a = { bg = colors.red, fg = colors.bg, gui = "bold" },
      b = { bg = colors.bg, fg = colors.fg },
      c = { bg = colors.bg, fg = colors.fg },
    },
    inactive = {
      a = { bg = colors.inactive_bg, fg = colors.semilightgray, gui = "bold" },
      b = { bg = colors.inactive_bg, fg = colors.semilightgray },
      c = { bg = colors.inactive_bg, fg = colors.semilightgray },
    },
  }

  lualine.setup {
    options = {
      jheme = lualine_theme,
    },
    sections = {
      lualine_x = {
        {
          lazy_status.updates,
          cond = lazy_status.has_updates,
          color = { fg = "#ff9e64" },
        },
        { -- rest.nvim
          "rest",
          icon = "",
          fg = "#428890",
        },
        { "encoding" },
        { "fileformat" },
        { "filetype" },
      },
    },
  }
end

return M
