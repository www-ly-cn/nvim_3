local M = {
  "rcarriga/nvim-notify",
  enabled = true,
  event = "VimEnter",
}

function M.config()
  local notify_status, notify = pcall(require, "notify")
  if not notify_status then
    vim.notify "Not found 'nvim-notify' plugin"
  end
  vim.opt.termguicolors = true

  notify.setup {
    -- render = "default",
    -- background_colour = "Normal",
    background_colour = "#000000",
    render = "minimal",
    top_down = false,
    -- "fade", "slide", "fade_in_slide_out", "static"
    stages = "static",
    on_open = nil,
    on_close = nil,
    timeout = 3000,
    fps = 1,
    max_width = math.floor(vim.api.nvim_win_get_width(0) / 2),
    max_height = math.floor(vim.api.nvim_win_get_height(0) / 4),
    -- minimum_width = 50,
    -- ERROR > WARN > INFO > DEBUG > TRACE
    level = "TRACE",
  }

  -- vim.notify("xxx", vim.log.levels.INFO, {})

  vim.notify = notify
end

return M
