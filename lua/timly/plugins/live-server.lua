return {
  -- sudo pacman -S noto-fonts-emoji
  "ngtuonghy/live-server-nvim",
  event = "VeryLazy",
  build = ":LiveServerInstall",
  config = function()
    --[[
      LiveServerStart--Run server
      LiveServerStart -f   -- Serve the currently open file (Note: for this to work, `open` mode in setup must be set to "folder")
      LiveServerStop --Stop server
      LiveServerToggle --Toggle server
    ]]
    require("live-server-nvim").setup {
      custom = {
        "--port=8080",
        "--no-css-inject",
      },
      serverPath = vim.fn.stdpath "data" .. "/live-server/", --default
      open = "folder", -- folder|cwd     --default
    }

    vim.keymap.set("n", "<leader>lt", function()
      require("live-server-nvim").toggle()
    end, { desc = "toggle live server (open/close)" })
  end,
}
