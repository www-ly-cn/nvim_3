--[[ local M = {
  -- Fuzzy Search
  "nvim-telescope/telescope.nvim",
  -- cmd = "Telescope",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
    "sharkdp/fd",
    "folke/todo-comments.nvim",
  },
}

function M.config()
  local telescope = require "telescope"
  local actions = require "telescope.actions"

  telescope.setup {
    defaults = {
      initial_mode = "insert",
      path_display = { "smart" },
      prompt_prefix = " ",
      selection_caret = " ",
      vimgrep_arguments = {
        "rg",
        "--color=never",
        "--no-heading",
        "--with-filename",
        "--line-number",
        "--column",
        "--smart-case",
        "--hidden",
        "--glob=!.git/",
      },
      mappings = {
        i = {
          -- move to prev result
          ["<C-k>"] = actions.move_selection_previous,
          -- move to next result
          ["<C-j>"] = actions.move_selection_next,
          -- 关闭
          ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        },
      },
    },

    extensions = {
      fzf = {
        fuzzy = true, -- false will only do exact matching
        override_generic_sorter = true, -- override the generic sorter
        override_file_sorter = true, -- override the file sorter
        case_mode = "smart_case", -- or "ignore_case" or "respect_case"
        -- the default case_mode is "smart_case"
      },
    },
  }

  telescope.load_extension "fzf"

  vim.keymap.set(
    "n",
    "<leader>tf",
    "<cmd>Telescope find_files<cr>",
    { desc = "Fuzzy find files in cwd" }
  )
  vim.keymap.set(
    "n",
    "<leader>tr",
    "<cmd>Telescope oldfiles<cr>",
    { desc = "Fuzzy find recent files" }
  )
  vim.keymap.set("n", "<leader>tc", "<cmd>Telescope live_grep<cr>", { desc = "Find string in cwd" })
  vim.keymap.set(
    "n",
    "<leader>ts",
    "<cmd>Telescope grep_string<cr>",
    { desc = "Find string under cursor in cwd" }
  )
  -- todo-comments  plugins
  vim.keymap.set("n", "<leader>tt", "<cmd>TodoTelescope<cr>", { desc = "Find todos" })
end

return M ]]

local M = {
  "nvim-telescope/telescope.nvim",
  -- commit = "",
  -- lazy = true,
  -- cmd = "Telescope",
  enable = true,
  dependencies = {
    {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
    },
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    "sharkdp/fd",

    "nvim-telescope/telescope-project.nvim",

    -- undo
    "debugloop/telescope-undo.nvim",
    -- DAP
    "nvim-telescope/telescope-dap.nvim",
    --[[
      :Telescope dap commands
      :Telescope dap configurations
      :Telescope dap list_breakpoints
      :Telescope dap variables
      :Telescope dap frames
      ]]
    -- lazygit
    "kdheepak/lazygit.nvim",
    -- aerial
    "stevearc/aerial.nvim", -- outline 被 Lspsaga 替代

    {
      "rest-nvim/rest.nvim",
      enabled = false,
      cmd = { "Rest" },
      dependencies = {
        {
          "vhyrro/luarocks.nvim",
          priority = 1000,
          config = true,
          opts = {
            rocks = { "lua-curl", "nvim-nio", "mimetypes", "xml2lua" },
          },
        },
        {
          "nvim-treesitter/nvim-treesitter",
          opts = function(_, opts)
            opts.ensure_installed = opts.ensure_installed or {}
            table.insert(opts.ensure_installed, "http")
          end,
        },
      },
      ft = "http",
      config = function()
        vim.g.rest_nvim = {
          -- ...
        }
        -- first load extension
        require("telescope").load_extension "rest"
        -- then use it, you can also use the `:Telescope rest select_env` command
        -- require("telescope").extensions.rest.select_env()
      end,
    },
  },
}

function M.config()
  local actions = require "telescope.actions"
  local builtin = require "telescope.builtin"

  -- ctrl + j/k 选择某一选项 tab 选择多个选项
  -- telescope 打开的 通过  ctrl + q 关闭

  -- #region telescope-fzf-native
  --- ============telescope-fzf-native begin================
  -- -- Global files search
  vim.keymap.set("n", "<leader>tf", builtin.find_files, { desc = "Search files" })
  -- Global words search
  vim.keymap.set("n", "<leader>ts", builtin.live_grep, { desc = "Search words [ripgrep[rg]]" }) -- pacman -S ripgrep 命令为 rg "xxx"
  vim.keymap.set("n", "<leader>tw", builtin.grep_string, { desc = "Grep string" })
  vim.keymap.set("n", "<leader>th", builtin.help_tags, { desc = "Search help" })
  vim.keymap.set("n", "<leader>td", builtin.diagnostics, { desc = "Search diagnostics" })
  vim.keymap.set("n", "<leader>tb", builtin.buffers, { desc = "Search existing buffers" })
  vim.keymap.set("n", "<leader>tr", builtin.oldfiles, { desc = "Searc recently opend files" })
  vim.keymap.set("n", "<leader>tc", function()
    local themes = require "telescope.themes"
    builtin.current_buffer_fuzzy_find(themes.get_dropdown {
      winblend = 10,
      previewer = false,
    })
  end, {
    desc = "Fuzzily search in current buffer",
  })
  -- outline -> Map
  vim.keymap.set("n", ";s", builtin.treesitter, { desc = "Lists Function names, variables, from Treesitter" })

  -- undo
  vim.keymap.set("n", "<leader>tu", ':lua require("telescope").extensions.undo.undo()<cr>', {
    desc = "Fuzzily search in undo history",
  })

  -- ===============================================================
  local aerial_status, aerial = pcall(require, "aerial")
  if not aerial_status then
    vim.notify "Not found 'aerial.nvim' plugin"
    return
  end

  aerial.setup {
    layout = {
      min_width = 20,
    },
    backends = { "lsp", "treesitter", "markdown" },
    close_behavior = "auto",
    default_bindings = true,
    default_direction = "prefer_right",
    on_attach = function(bufnr)
      -- Jump forwards/backwards with '{' and '}'
      vim.keymap.set("n", "<Up>", "<cmd>AerialPrev<CR>", { buffer = bufnr, desc = "Map Outline Prev" })
      vim.keymap.set("n", "<Down>", "<cmd>AerialNext<CR>", { buffer = bufnr, desc = "Map Outline Next" })
    end,
  }

  -- You probably also want to set a keymap to toggle aerial   右侧map  outline
  vim.keymap.set("n", "<leader>ol", "<cmd>AerialToggle!<CR>", { desc = "Right Document Map" })
  vim.keymap.set("n", "<leader>ta", "<cmd>Telescope aerial<CR>", { desc = "Telescope Document Map" })

  -- undo
  vim.keymap.set("n", "<leader>tu", ':lua require("telescope").extensions.undo.undo()<cr>', {
    desc = "Fuzzily search in undo history",
  })
  -- ===============================================================

  local telescope = require "telescope"

  telescope.setup {
    defaults = {
      prompt_prefix = " ",
      selection_caret = " ",
      entry_prefix = "   ",
      initial_mode = "insert",
      selection_strategy = "reset",
      color_devicons = true,
      set_env = { ["COLORTERM"] = "truecolor" },
      path_display = { "smart" },
      sorting_strategy = nil,
      layout_strategy = nil,
      layout_config = {},
      vimgrep_arguments = {
        "rg",
        "--color=never",
        "--no-heading",
        "--with-filename",
        "--line-number",
        "--column",
        "--smart-case",
        "--hidden",
        "--glob=!.git/",
      },

      mappings = {
        i = {
          ["<C-n>"] = actions.cycle_history_next,
          ["<C-p>"] = actions.cycle_history_prev,

          ["<C-j>"] = actions.move_selection_next,
          ["<C-k>"] = actions.move_selection_previous,
        },
        n = {
          ["<esc>"] = actions.close,
          ["j"] = actions.move_selection_next,
          ["k"] = actions.move_selection_previous,
          ["q"] = actions.close,
          ["<C-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        },
      },
    },
    pickers = {
      live_grep = {
        theme = "dropdown",
      },

      grep_string = {
        theme = "dropdown",
      },

      find_files = {
        theme = "dropdown",
        previewer = false,
      },

      buffers = {
        theme = "dropdown",
        previewer = false,
        initial_mode = "normal",
        mappings = {
          i = {
            ["<C-d>"] = actions.delete_buffer,
          },
          n = {
            ["dd"] = actions.delete_buffer,
          },
        },
      },

      planets = {
        show_pluto = true,
        show_moon = true,
      },

      colorscheme = {
        enable_preview = true,
      },

      lsp_references = {
        theme = "dropdown",
        initial_mode = "normal",
      },

      lsp_definitions = {
        theme = "dropdown",
        initial_mode = "normal",
      },

      lsp_declarations = {
        theme = "dropdown",
        initial_mode = "normal",
      },

      lsp_implementations = {
        theme = "dropdown",
        initial_mode = "normal",
      },
    },
    extensions = {
      fzf = {
        fuzzy = true,                   -- false will only do exact matching
        override_generic_sorter = true, -- override the generic sorter
        override_file_sorter = true,    -- override the file sorter
        case_mode = "smart_case",       -- or "ignore_case" or "respect_case"
      },
    },
  }

  -- fzf
  pcall(telescope.load_extension, "fzf")

  pcall(telescope.load_extension, "project")

  -- -- undo
  -- pcall(telescope.load_extension, "undo")
  --
  -- -- debug
  -- pcall(telescope.load_extension, "dap")
  --
  -- -- lazygit
  -- pcall(telescope.load_extension, "lazygit")

  -- aerial
  pcall(telescope.load_extension, "aerial") -- 文件右侧 map 导航(文件大纲)

  -- postman client http
  -- first load extension
  -- require("telescope").load_extension("rest")
  -- pcall(telescope.load_extension, "rest") -- 文件右侧 map 导航(文件大纲)
  -- then use it, you can also use the `:Telescope rest select_env` command
  -- telescope.extensions.rest.select_env()
end

return M
