return {
  "tamton-aquib/duck.nvim",
  event = "VeryLazy",
  config = function()
    -- vim.keymap.set("n", "<leader>dd", function()
    --   require("duck").hatch()
    -- end, {})
    vim.keymap.set("n", "<leader>ek", function()
      require("duck").cook()
    end, {})
    vim.keymap.set("n", "<leader>ea", function()
      require("duck").cook_all()
    end, {})

    -- 🦆 ඞ 🦀 🐈 🐎 🦖 🐤
    vim.keymap.set("n", "<leader>ed", function()
      require("duck").hatch("🦆", 10)
    end, {}) -- A pretty fast duck
    vim.keymap.set("n", "<leader>ec", function()
      require("duck").hatch("🐈", 0.75)
    end, {}) -- Quite a mellow cat
  end,
}
