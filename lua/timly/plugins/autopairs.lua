-- local M = {
--   "windwp/nvim-autopairs",
--   enabled = true,
--   event = "InsertEnter",
--   dependencies = {
--     "hrsh7th/nvim-cmp",
--   },
-- }
--
-- function M.config()
--   local autopairs = require("nvim-autopairs")
--
--
--   -- configure autopairs
--   autopairs.setup({
--     check_ts = true, -- enable treesitter
--     ts_config = {
--       lua = { "string" }, -- don't add pairs in lua string treesitter nodes
--       javascript = { "template_string" }, -- don't add pairs in javscript template_string treesitter nodes
--       java = false, -- don't check treesitter on java
--     },
--   })
--
--   -- import nvim-autopairs completion functionality
--   local cmp_autopairs = require("nvim-autopairs.completion.cmp")
--
--   -- import nvim-cmp plugin (completions plugin)
--   local cmp = require("cmp")
--
--   -- make autopairs and completion work together
--   cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
-- end
--
-- return M

local M = {
  {
    "windwp/nvim-autopairs",
    enabled = true,
    event = "InsertEnter",

    dependencies = {
      "hrsh7th/nvim-cmp",
    },
    config = function()
      -- autopairs
      local status_npairs, npairs = pcall(require, "nvim-autopairs")
      if not status_npairs then
        vim.notify "Not found 'nvim-autopairs' plugin"
        return
      end

      local Rule_status, Rule = pcall(require, "nvim-autopairs.rule")
      if not Rule_status then
        vim.notify "Not found 'nvim-autopairs.rule' plugin"
      else
        npairs.setup {
          check_ts = true,
          ts_config = {
            lua = { "string" }, -- it will not add a pair on that treesitter node
            javascript = { "template_string" },
            java = false, -- don't check treesitter on java
          },
        }
      end

      -- 需要 nvim-treesitter 插件配合使用
      local ts_conds_status, ts_conds = pcall(require, "nvim-autopairs.ts-conds")
      if not ts_conds_status then
        vim.notify "Not found 'nvim-autopairs.ts-conds' plugin"
      else
        -- press % => %% only while inside a comment or string
        npairs.add_rules {
          Rule("%", "%", "lua"):with_pair(ts_conds.is_ts_node { "string", "comment" }),
          Rule("$", "$", "lua"):with_pair(ts_conds.is_not_ts_node { "function" }),
        }
      end

      -- 依赖插件 nvim-autopairs
      local cmp_autopairs = require "nvim-autopairs.completion.cmp"
      -- 在选择函数或方法项后插入“（”
      require("cmp").event:on("confirm_done", cmp_autopairs.on_confirm_done())

      npairs.setup {
        check_ts = true,
        disable_filetype = { "TelescopePrompt", "spectre_panel" },
      }
    end,
  },
  {
    -- 简单一些
    "m4xshen/autoclose.nvim",
    enabled = false,
    event = "InsertEnter",
    config = function()
      require("autoclose").setup()
    end,
  },
}

return M
