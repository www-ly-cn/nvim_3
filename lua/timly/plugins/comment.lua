local M = {
  "numToStr/Comment.nvim",
  enabled = true,
  event = { "BufReadPre", "BufNewFile" },
  -- keys = {
  --   { 'mgc', mode = { 'n', 'v' }, 'gcc' },
  -- },
  dependencies = {
    -- 依赖 nvim-treesiter 插件才能使用，增强的注释插件
    "JoosepAlviste/nvim-ts-context-commentstring",
  },
}

function M.config()
  local comment = require "Comment"
  local ts_context_commentstring = require "ts_context_commentstring.integrations.comment_nvim"

  comment.setup {
    -- pre_hook = ts_context_commentstring.create_pre_hook(),
    ---Function to call before (un)comment
    -- pre_hook = commentstring.create_pre_hook(),
    pre_hook = function(ctx)
      local loaded, ts_comment =
        pcall(require, "ts_context_commentstring.integrations.comment_nvim")
      if loaded and ts_comment then
        return ts_comment.create_pre_hook()(ctx)
      else
        vim.notify "Not found 'nvim-ts-context-commentstring' plugin"
      end
    end,
  }
end

return M
