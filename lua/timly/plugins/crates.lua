return {
  "saecki/crates.nvim",
  ft = { "toml" },
  event = { "BufRead Cargo.toml" },
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  config = function()
    local crates = require "crates"
    crates.setup()
  end,
}
