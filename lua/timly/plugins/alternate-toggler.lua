return {
  "rmagatti/alternate-toggler",
  event = { "BufReadPre", "BufNewFile" },
  enabled = true,
  config = function()
    -- true/false这种的快速修改
    -- ["true"] = "false",
    -- ["True"] = "False",
    -- ["TRUE"] = "FALSE",
    -- ["Yes"] = "No",
    -- ["YES"] = "NO",
    -- ["1"] = "0",
    -- ["<"] = ">",
    -- ["("] = ")",
    -- ["["] = "]",
    -- ["{"] = "}",
    -- ['"'] = "'",
    -- ['""'] = "''",
    -- ["+"] = "-",
    -- ["==="] = "!=="

    require("alternate-toggler").setup {
      alternates = {
        ["TRUE"] = "FALSE",
        ["error"] = "warn",
        ["_"] = "-",
      },
    }
    vim.keymap.set("n", "<Space>ta", "<cmd>ToggleAlternate<cr>")
  end,
}
