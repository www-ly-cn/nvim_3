local M = {
  -- 自动补全成对的标签 HTML

  --[[
      Before        Input         After
      ------------------------------------
      <div           >              <div></div>
      <div></div>    ciwspan<esc>   <span></span>
      ------------------------------------ 
    ]]
  "windwp/nvim-ts-autotag",
  enabled = true,
  event = "InsertEnter",
}

function M.config()
  local autotag_status, autotag = pcall(require, "nvim-ts-autotag")
  if not autotag_status then
    vim.notify "Not found 'nvim-ts-autotag' plugin"
    return
  end
  -- 可以不用在 treesiter 中配置
  autotag.setup {
    opts = {
      -- Defaults
      enable_close = true, -- Auto close tags
      enable_rename = true, -- Auto rename pairs of tags
      enable_close_on_slash = true, -- Auto close on trailing </
    },
    -- Also override individual filetype configs, these take priority.
    -- Empty by default, useful if one of the "opts" global settings
    -- doesn't work well in a specific filetype
    per_filetype = {
      ["html"] = {
        enable_close = true,
      },
    },
    filetypes = {
      "javascript",
      "typescript",
      "javascriptreact",
      "typescriptreact",
      "svelte",
      "vue",
      "tsx",
      "jsx",
      "rescript",
      "html",
      "xml",
      "php",
      "markdown",
      "handlebars",
      "hbs",
      "rust",
      "rs",
      "python",
      "py",
    },
  }
end

return M
