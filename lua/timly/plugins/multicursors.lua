return {
  {
    "smoka7/multicursors.nvim",
    event = "VeryLazy",
    dependencies = {
      "nvimtools/hydra.nvim",
    },
    opts = {},
    cmd = {
      "MCstart",
      "MCvisual",
      "MCclear",
      "MCpattern",
      "MCvisualPattern",
      "MCunderCursor",
    },
    keys = {
      {
        mode = { "v", "n" },
        "<Space>m",
        "<cmd>MCstart<cr>",
        desc = "Create a selection for selected text or word under the cursor",
      },
    },

    config = function()
      require("multicursors").setup {
        hint_config = false, -- 是否开启动提示面板
        -- hint_config = {
        --     float_opts = {
        --         border = 'rounded',
        --     },
        --     position = 'bottom-right',
        -- },
        -- generate_hints = {
        --     normal = true,
        --     insert = true,
        --     extend = true,
        --     config = {
        --         column_count = 1,
        --     },
        -- },
      }
    end,
  },

  {
    -- 多光标操作，在 V-Block 模式下，选中后 Ctrl+N 依次选中其他一致的单词
    "mg979/vim-visual-multi",
    event = { "BufReadPre", "BufNewFile" },
    enabled = true,
  },
}
