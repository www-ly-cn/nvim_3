local M = {
  "kelly-lin/ranger.nvim",
  event = "VeryLazy",
  enabled = true,
}

function M.config()
  local ranger_nvim = require "ranger-nvim"

  ranger_nvim.setup {
    enable_cmds = false,
    replace_netrw = false,
    keybinds = {
      ["ov"] = ranger_nvim.OPEN_MODE.vsplit,
      ["oh"] = ranger_nvim.OPEN_MODE.split,
      ["ot"] = ranger_nvim.OPEN_MODE.tabedit,
      ["or"] = ranger_nvim.OPEN_MODE.rifle,
    },
    ui = {
      border = "none",
      height = 0.8,
      width = 0.8,
      x = 0.5,
      y = 0.5,
    },
  }

  vim.api.nvim_set_keymap("n", "<space>rg", "", {
    noremap = true,
    callback = function()
      require("ranger-nvim").open(true)
    end,
  })
end

return M
