return {
  -- 字符串中输入 ${} 自动处理成模板字符
  "axelvc/template-string.nvim",
  event = "InsertEnter",
  ft = { "typescript", "javascript", "typescriptreact", "javascriptreact", "vue" },
  opts = {
    filetypes = {
      "html",
      "typescript",
      "javascript",
      "typescriptreact",
      "javascriptreact",
      "vue",
      "svelte",
      "python",
    }, -- filetypes where the plugin is active
    jsx_brackets = true, -- must add brackets to JSX attributes
    remove_template_string = true, -- remove backticks when there are no template strings
    restore_quotes = {
      -- quotes used when "remove_template_string" option is enabled
      normal = [[']],
      jsx = [["]],
    },
  },
}
