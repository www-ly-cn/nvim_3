local M = {
  -- 漂亮的成对括号
  "HiPhish/rainbow-delimiters.nvim",
  enabled = true,
  event = { "BufReadPre", "BufNewFile" },
}

function M.config()
  local rainbow_delimiters_status, rainbow_delimiters = pcall(require, "rainbow-delimiters")
  if not rainbow_delimiters_status then
    vim.notify "Not found 'rainbow-delimiters.nvim' plugin"
  end

  vim.g.rainbow_delimiters = {
    strategy = {
      [""] = rainbow_delimiters.strategy["global"],
      vim = rainbow_delimiters.strategy["local"],
    },
    query = {
      [""] = "rainbow-delimiters",
      lua = "rainbow-blocks",
    },
    highlight = {
      "RainbowDelimiterRed",
      "RainbowDelimiterYellow",
      "RainbowDelimiterBlue",
      "RainbowDelimiterOrange",
      "RainbowDelimiterGreen",
      "RainbowDelimiterViolet",
      "RainbowDelimiterCyan",
    },
  }
end

return M
