local M = {
  -- 快速复制向上向下粘贴
  "hinell/duplicate.nvim",
  event = "VeryLazy",
}

function M.config()
  vim.keymap.set("n", "<C-S-Up>", "<Cmd>LineDuplicate -1<CR>", { desc = "Line: duplicate up" })
  vim.keymap.set("n", "<C-S-Down>", "<Cmd>LineDuplicate +1<CR>", { desc = "Line: duplicate down" })
  vim.keymap.set(
    "v",
    "<C-S-Up>",
    "<Cmd>VisualDuplicate -1<CR>",
    { desc = "Selection: duplicate up" }
  )
  vim.keymap.set(
    "v",
    "<C-S-Down>",
    "<Cmd>VisualDuplicate +1<CR>",
    { desc = "Selection: duplicate down" }
  )
end

return M
