local M = {
  "nvim-tree/nvim-tree.lua",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
  },
}

function M.config()
  local nvim_tree_status, nvim_tree = pcall(require, "nvim-tree")
  if not nvim_tree_status then
    vim.notify "Not found 'nvim-tree.lua' plugin"
  end

  -- disable netrw at the very start of your init.lua
  -- vim.g.loaded = 1
  vim.g.loaded_netrw = 1
  vim.g.loaded_netrwPlugin = 1
  -- optionally enable 24-bit colour
  vim.opt.termguicolors = true

  local keymap = vim.keymap.set
  keymap(
    { "n", "v" },
    "<leader>ee",
    "<cmd>NvimTreeToggle<cr>",
    { desc = "Toggle file explorer", noremap = true, silent = true }
  )
  keymap(
    { "n", "v" },
    "<leader>eo",
    "<cmd>NvimTreeFocus<cr>",
    { desc = "Toggle file explorer on focus file", noremap = true, silent = true }
  )
  keymap(
    { "n", "v" },
    "<leader>ef",
    "<cmd>NvimTreeFindFile<cr>",
    { desc = "Toggle file explorer on current file", noremap = true, silent = true }
  )
  keymap(
    { "n", "v" },
    "<leader>ec",
    "<cmd>NvimTreeCollapse<cr>",
    { desc = "Collapse file explorer", noremap = true, silent = true }
  )
  keymap(
    { "n", "v" },
    "<leader>er",
    "<cmd>NvimTreeRefresh<cr>",
    { desc = "Refresh file explorer", noremap = true, silent = true }
  )

  nvim_tree.setup {
    view = {
      centralize_selection = false,
      side = "left",
      width = 35,
      relativenumber = false,
    },
    renderer = {
      indent_markers = {
        enable = true,
      },
      icons = {
        git_placement = "after", -- before after
        modified_placement = "before",
        glyphs = {
          folder = {
            arrow_closed = "",
            arrow_open = "",
            -- arrow_closed = "",
            -- arrow_open = ""
            -- arrow_closed = "",
            -- arrow_open = "",
          },
          git = {
            unstaged = "✗",
            staged = "✓",
            unmerged = "",
            renamed = "➜",
            untracked = "★",
            deleted = "",
            ignored = "◌",
          },
        },
      },
    },
    actions = {
      open_file = {
        -- 首次打开大小适配
        resize_window = false,
        -- 打开文件时不关闭
        quit_on_open = true,
        window_picker = {
          enable = false,
        },
      },
    },

    filters = {
      dotfiles = false,
      custom = {
        "^.git$",
        "^node_modules$",
        "^target$", -- rust
        "^.vscode$",
        "^.DS_Store$",
      },
    },
    git = {
      -- enable = true,
      ignore = false,
    },

    -- keymap override
    on_attach = function(buffer_id)
      local api = require "nvim-tree.api"

      local function opts(desc)
        return {
          desc = "nvim-tree: " .. desc,
          buffer = buffer_id,
          noremap = true,
          silent = true,
          nowait = true,
        }
      end
      -- use add default mappings
      api.config.mappings.default_on_attach(buffer_id)

      vim.keymap.set("n", "sv", api.node.open.vertical, opts "Open: Vertical Split")
      vim.keymap.set("n", "sh", api.node.open.horizontal, opts "Open: Horizontal Split")
    end,
  }
end

return M
