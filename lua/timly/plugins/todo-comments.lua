return {
  "folke/todo-comments.nvim",
  enabled = true,
  event = { "BufReadPre", "BufNewFile" },
  dependencies = "nvim-lua/plenary.nvim",
  config = function()
    -- 注释的颜色高亮注释
    local todo_comments = require("todo-comments")

    vim.keymap.set("n", "]t", function() 
      todo_comments.jump_next()
    end, { desc = "Next todo comment" })
    vim.keymap.set("n", "[t", function() 
      todo_comments.jump_prev()
    end, { desc = "Previous todo comment" })
    -- 在 telescope 中配置一下更好使用
    todo_comments.setup()
    -- TODO:
    -- HACK: 
    -- BUG: 
  end,
}

