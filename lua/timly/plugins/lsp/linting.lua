return {
  "mfussenegger/nvim-lint",
  event = { "BufReadPre", "BufNewFile" },
  enabled = true,
  config = function()
    require("lint").linters_by_ft = {
      -- lua = { "luacheck" },

      javascript = { "eslint" },
      javascriptreact = { "eslint" },
      typescript = { "eslint" },
      typescriptreact = { "eslint" },
      svelte = { "eslint" },

      -- javascript = { "eslint_d" },
      -- javascriptreact = { "eslint_d" },
      -- typescript = { "eslint_d" },
      -- typescriptreact = { "eslint_d" },
      -- svelte = { "eslint_d" },

      python = { "pylint" },
      markdown = { "markdownlint" },
    }

    local lint_augroup = vim.api.nvim_create_augroup("lint", { clear = true })
    -- { "BufEnter", "BufWritePost", "InsertLeave", "TextChanged", "BufReadPre" }
    vim.api.nvim_create_autocmd({ "BufWritePost" }, {
      group = lint_augroup,
      callback = function()
        require("lint").try_lint()
      end,
    })

    vim.keymap.set("n", "<leader>fl", function()
      require("lint").try_lint()
    end, { desc = "Trigger linting for current file" })
  end,
}
