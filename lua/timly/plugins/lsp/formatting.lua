local M = {
  -- Formatting
  "stevearc/conform.nvim",
  enabled = true,
  event = { "BufReadPre", "BufNewFile" },
}

function M.config()
  local conform_status, conform = pcall(require, "conform")
  if not conform_status then
    vim.notify "Not found 'conform.nvim' plugin"
    return
  end

  -- Conform also provides a formatexpr, same as the LSP client
  -- vim.o.formatexpr = "v:lua.require'conform'.formatexpr()"

  conform.setup {
    formatters_by_ft = {
      lua = { "stylua" },
      graphql = { "prettier" },
      liquid = { "prettier" },
      c = { "clang_format" },
      cpp = { "clang_format" },
      -- cmake = { "cmake_format" },
      -- rust = { "rustfmt", lsp_format = "fallback" },-- rustfmt deprecated
      bash = { "shfmt" },
      go = { "gofumpt", "goimports" },
      yaml = { "prettier" },
      json = { "jq", "prettier" },
      markdown = { "prettierd" },
      -- config will use the first available formatter int the list
      html = { "prettierd" },
      css = { "prettierd" },
      svelte = { "prettierd" },
      javascript = { "prettierd" },
      typescript = { "prettierd" },
      javascriptreact = { "prettierd" },
      typescriptreact = { "prettierd" },
      vue = { "prettierd" },
      -- Formatters can slso be specified with additional options
      python = function(bufnr)
        if require("conform").get_formatter_info("ruff_format", bufnr).available then
          return { "ruff_format" }
        else
          return { "isort", "black" }
        end
      end,
      zig = { "zigfmt" },
    },
    formatters = {
      clang_format = {
        prepend_args = { "--style=file", "--fallback-style=LLVM" },
      },
      shfmt = {
        prepend_args = { "-i", "4" },
      },
    },
    format_on_save = {
      -- These options will be passed to conform.format()
      timeout_ms = 500,
      lsp_format = "fallback",
      async = false,
    },
    log_level = vim.log.levels.ERROR,
  }

  vim.api.nvim_create_autocmd({ "BufWritePre" }, {
    pattern = "*",
    callback = function(args)
      conform.format { bufnr = args.buf }
    end,
  })

  vim.keymap.set({ "n", "v" }, "<leader>fc", function()
    conform.format {
      lsp_fallback = true,
      async = true,
      timeout_ms = 1000,
    }
  end, { desc = "Format file or range (in visual mode)" })
end

return M
