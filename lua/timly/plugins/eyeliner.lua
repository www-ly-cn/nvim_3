return {
  "jinh0/eyeliner.nvim",
  enabled = true,
  config = function()
    --  f/F/t/T,
    vim.api.nvim_set_hl(0, "EyelinerPrimary", { fg = "#ff0000", bold = true, underline = true })
    vim.api.nvim_set_hl(0, "EyelinerSecondary", { fg = "#ffffff", underline = true })

    require("eyeliner").setup {
      highlight_on_key = true, -- show highlights only after keypress
      -- dim = false              -- dim all other characters if set to true (recommended!)
    }
  end,
}
