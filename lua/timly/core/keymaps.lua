-- vim 的原生文档树, 使用 nvim-tree 替代 netrwPlugin
-- vim.keymap.set('', '<leader>e', '<cmd>Lex 30<cr>')
-- vim.keymap.set("", "<leader>e", "<cmd>Ex<cr>")

-- jk 替代 Esc
vim.keymap.set("i", "jk", "<Esc>", { desc = "Exit insert mode with jk" })

-- 禁用方向键
-- vim.keymap.set({ "n", "i" }, "<Left>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Right>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Up>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Down>", "<Nop>")

-- 当超过编辑器的边界视觉上换行了，其实还是一行时，跳行的以实际算一行计算
-- https://www.reddit.com/r/vim/comments/2k4cbr/problem_with_gj_and_gk/
vim.keymap.set("n", "j", [[v:count ? 'j' : 'gj']], { noremap = true, expr = true })
vim.keymap.set("n", "k", [[v:count ? 'k' : 'gk']], { noremap = true, expr = true })

-- 禁用方向键
-- vim.keymap.set({ "n", "i" }, "<Left>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Right>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Up>", "<Nop>")
-- vim.keymap.set({ "n", "i" }, "<Down>", "<Nop>")

--- Mappings for buffers
vim.keymap.set("n", "<leader>bca", "<cmd>bufdo bdelete<cr>", { desc = "[B]uffers [C]lose [A]ll" })
vim.keymap.set("n", "<leader>bco", "<cmd>%bd|e#<cr>", { desc = "[B]uffers [C]lose [O]thers" })

-- Copy current buffer file path to clipboard
vim.keymap.set("n", "<leader>cpr", '<cmd>let @+=expand("%")<cr>', { desc = "[C]opy buffer [R]elative file [P]ath" })
vim.keymap.set("n", "<leader>cpa", '<cmd>let @+=expand("%:p")<cr>', { desc = "[C]opy buffer [A]bsolute file [P]ath" })
vim.keymap.set("n", "<leader>cpf", '<cmd>let @+=expand("%:t")<cr>', { desc = "[C]opy buffer [F]ile name [P]ath" })
vim.keymap.set("n", "<leader>cpd", '<cmd>let @+=expand("%:p:h")<cr>', { desc = "[C]opy buffer [D]ir name [P]ath" })

-- Cancel s default function
-- 取消掉默认的快捷键功能
-- vim.keymap.set("n", "s", "<Nop>")
-- 取消掉默认的快捷键x的剪切到寄存器的功能
-- vim.keymap.set("n", "x", '"_x')

-- Save
vim.keymap.set({ "i", "n" }, "<leader>w", "<Esc><cmd>write<cr>")
vim.keymap.set({ "i", "n" }, "<C-s>", "<Esc><cmd>write<cr>")

-- Exit
---- ZZ 保存已改动的文件，并关闭退出窗口
-- map("n", "qq", "<cmd>q!<CR>")
-- map("n", "Q", "<cmd>qa!<CR>")
-- map("n", "q", "<cmd>q<CR>") -- q 会和 lazy 的冲突,所以配置成 <leader>q
vim.keymap.set("n", "<C-q>", "<CMD>wq<CR>") -- 保存已改动的文件，并关闭退出窗口

-- Close current
-- vim.keymap.set("n", "<leader>sc", "<C-w>q")
-- Close other
-- vim.keymap.set("n", "<leader>so", "<C-w>o")
-- Close all
vim.keymap.set({ "n", "v" }, "<leader>sa", "<C-w>o<cmd>q<cr>")

-- Select all
vim.keymap.set("n", "<C-a>", "gg<S-v>G", { desc = "Select all" })

-- clear highlight
vim.keymap.set("n", "<leader>n", "<cmd>nohl<cr>", { desc = "Clear search highlights" })

-- increment/decrement numbers
vim.keymap.set("n", "<leader>+", "<C-a>", { desc = "Increment number" })
vim.keymap.set("n", "<leader>-", "<C-x>", { desc = "Decreamen number" })

-- window management
vim.keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" })
vim.keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" })
vim.keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" })
vim.keymap.set("n", "<leader>sx", "<cmd>close<cr>", { desc = "Close current split window" })

vim.keymap.set("n", "<leader>to", "<cmd>tabnew<cr>", { desc = "Open new tab" })
vim.keymap.set("n", "<leader>tx", "<cmd>tabclose<cr>", { desc = "Close current tab" })
vim.keymap.set("n", "<leader>tn", "<cmd>tabn<cr>", { desc = "Go to next tab" })
vim.keymap.set("n", "<leader>tp", "<cmd>tabp<cr>", { desc = "Go to previous tab" })
vim.keymap.set("n", "<leader>tf", "<cmd>tabnew %<cr>", { desc = "Open current buffer in new tab" })

-- 光标回到行首
vim.keymap.set("n", "H", "^") -- map("n", "H", "0")
-- 光标回到行未
vim.keymap.set("n", "L", "$")

-- Delete characters for forward
vim.keymap.set("n", "de", 'vb"_d')
-- Delete characters for backwards
vim.keymap.set("n", "dw", 've"_d')

-- -- copy & paste
-- vim.keymap.set("v", "<C-Ins>", "\"+yi")
-- vim.keymap.set("v", "<S-Del>", "\"+c")
-- vim.keymap.set("v", "<S-Ins>", "c<ESC>\"+gp")
-- vim.keymap.set("i", "<S-Ins>", "<ESC>\"+gp" )
--
-- Mappigs for yank to clipboard
-- vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]], { desc = '[y] Yank to clipboard' })
-- vim.keymap.set("n", "<leader>Y", [["+Y]], { desc = '[Y] Yank line to clipboard' })

-- Configure Copy Shortcuts
vim.keymap.set("v", "<C-c>", '"+y') -- copy
-- map("i", "<LeftRelease>", '"+ygv') -- Paste with middle mouse click
-- map("n", "<LeftRelease>", '"+y')   -- Paste with middle mouse click
vim.keymap.set("v", "<C-x>", '"+d') -- cut
vim.keymap.set("n", "<C-v>", '"+p') -- paste from system clipboard
vim.keymap.set("i", "<C-v>", '<ESC>"+pa') -- paste from system clipboard

-- 使用 <Shift-u> 将光标所在单词转化为全大写
vim.keymap.set({ "i", "v", "n" }, "<C-S-u>", "<ESC>viwU") -- 光标跳到本词开始
-- vim.keymap.set({ "i", "v", "n" }, "<S-C-u>", "<ESC>viwUw") -- 光标跳到本词末尾
-- 使用 <Shift-l> 将光标所在单词转化为全小写
vim.keymap.set({ "i", "v", "n" }, "<C-S-l>", "<ESC>viwu")
-- vim.keymap.set({ "i", "v", "n" }, "<S-C-l>", "<ESC>viwuw")

vim.keymap.set("n", "<leader>cp", function()
  vim.notify(vim.api.nvim_buf_get_name(0))
end, { desc = "Copy Current buffer file path" })

-- ciw - 修改单词并插入
-- caw - 修改当前单词及后面的空格并插入
-- daw - 删除一个词不进入插入模式
-- diw - 删除一个词进入插入模式
-- d0 - 向前删除
-- d$ 或 D - 向后删除
-- de - 向前删除一些字符
-- dw - 向后删除一些字符
-- dt字符 - 删除当前光标到指定字符之间的内容

-- Alt + hjkl jump between windows (split)
-- split 分割的窗口间切换
-- vim.keymap.set({ "n", "i" }, "<A-left>", "<C-w>h")  -- left
-- vim.keymap.set({ "n", "i" }, "<A-right>", "<C-w>l") -- right
-- vim.keymap.set({ "n", "i" }, "<A-up>", "<C-w>k")    --  up
-- vim.keymap.set({ "n", "i" }, "<A-down>", "<C-w>j")  -- down
-- vim.keymap.set({ "n", "i" }, "<C-S-h>", "<C-w>h") -- left
-- vim.keymap.set({ "n", "i" }, "<C-S-l>", "<C-w>l") -- right
-- vim.keymap.set({ "n", "i" }, "<C-S-k>", "<C-w>k") --  up
-- vim.keymap.set({ "n", "i" }, "<C-S-j>", "<C-w>j") -- down

-- vim.keymap.set({ "n", "i" }, "<A-h>", "<C-w>h") -- left
-- vim.keymap.set({ "n", "i" }, "<A-l>", "<C-w>l") -- right
-- vim.keymap.set({ "n", "i" }, "<A-k>", "<C-w>k") --  up
-- vim.keymap.set({ "n", "i" }, "<A-j>", "<C-w>j") -- down

local opt = { noremap = true, silent = true }
local function map(mode, lhs, rhs)
  vim.api.nvim_set_keymap(mode, lhs, rhs, opt)
end
-- Scroll up and down
-- 上下移动光标 Ctrl + jk
-- map("n", "<A-k>", "4k")
-- map("n", "<A-j>", "4j") -- <C-j> 与 "hrsh7th/cmp-nvim-lsp" 有冲突
-- Ctrl u / ctrl + d move only 9 lines, half screen by default
-- Ctrl + ud 只移动10行，默认移动半屏
-- map("n", "<C-u>", "10k")
-- map("n", "<C-d>", "10j")

-- Left and right proportional control
-- map("n", "s,", "<cmd>vertical resize -20<CR>")
-- map("n", "s.", "<cmd>vertical resize +20<CR>")
map("n", "<C-Left>", "<cmd>vertical resize -2<CR>")
map("n", "<C-Right>", "<cmd>vertical resize +2<CR>")
-- Up and down ratio
-- map("n", "sj", "<cmd>resize +10<CR>")
-- map("n", "sk", "<cmd>resize -10<CR>")
map("n", "<C-Down>", "<cmd>resize +2<CR>")
map("n", "<C-Up>", "<cmd>resize -2<CR>")
-- Ratio
map("n", "<Space>s=", "<C-w>=")

-- Terminal related
-- map("n", "<leader>t", "<cmd>sp | terminal<CR>")
map("n", "<leader>vt", "<cmd>vsp | terminal<CR>")

-- Indent code in visual mode
-- map("v", "<", "<gv")
-- map("v", ">", ">gv")

-- 单行或多行移动
-- M 表示 Alt 键。Up，Down 是方向键上下
-- map("v", "<M-Down>", "<cmd>m '>+1<CR>gv=gv")
-- map("v", "<M-Up>", "<cmd>m '<-2<CR>gv=gv")
map("v", "<A-Down>", "<cmd>m '>+1<CR>gv=gv")
map("v", "<A-Up>", "<cmd>m '<-2<CR>gv=gv")
-- Move selected text up and down
-- map("v", "J", "<cmd>move '>+1<CR>gv-gv")
-- map("v", "K", "<cmd>move '<-2<CR>gv-gv")
-- map("n", "<A-Down><Left>", "<cmd>m '>+1<CR>gv=gv")

-- 导航栏切换, 从 nvim-tree 跳到 bufferline 的 buffer 上
map("n", "<A-Tab>", "<cmd>bNext<CR>")
-- 关闭 tab， 可以交给 bufferline 中的插件 "famiu/bufdelete.nvim" 处理，
map("n", "<leader>bd", "<cmd>bd<CR>")

-- 在所有缓冲区(buffers)之间循环
map("n", "<PageUp>", "<CMD>bprevious<CR>")
map("n", "<PageDown>", "<CMD>bnext<CR>")
-- bufferline.nvim
-- bufferline 与 nvim-tree 之间的切换, 也就是顶部文件之间的切换
-- vim.keymap.set("n", "<leader>l", "<cmd>bnext<CR>")
-- vim.keymap.set("n", "<leader>h", "<cmd>bprevious<CR>")

-- ctrl + o 与 ctrl + i  来回跳
-- buffer 的选择
map("n", "<leader>bs", "<cdm>buffers<cr><cmd>b<space>")
-- CTRL + o：调整到上一次光标所在的行号上，即往后跳转
-- CTRL + i ：调整到jump list中当前记录的下一个记录的行号上，即往前调整

-- pacman -S lazygit
-- -- 映射 alt + g 快捷键以打开 lazygit
map("n", "<M-g>", "<cmd>new<CR><cmd>term lazygit<CR>i")

-- 在编辑的时候通过 Ctrl + z 暂时退出 nvim 的边界，做其他工作，
-- 其他操作完成后，在命令行通过 fg 命令回到之前 nvim 的工作界面
map("n", "<Space>wl", "<cmd>lua vim.wo.wrap = not vim.wo.wrap<CR>")
