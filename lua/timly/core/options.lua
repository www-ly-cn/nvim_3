-- Enable line numbers
vim.api.nvim_win_set_option(0, "number", true)
vim.api.nvim_win_set_option(0, "relativenumber", true)

vim.g.encoding = "utf-8"
vim.g.fileencoding = "utf-8"
vim.g.termencoding = "utf-8"

-- update interval for gitsigns
vim.o.updatetime = 100
-- keymap timeout
vim.o.timeoutlen = 500

-- 末行模式命令补全增强
vim.o.wildmenu = true

local options = {
  number = true,
  relativenumber = true,

  history = 1000,

  autoread = true,
  autowrite = true,

  -- spell = true,
  -- spelllang = { "en_us", "cjk" },

  conceallevel = 0,

  -- 1 不显示 2 显示  会影响 bufferline 的是否显示
  showtabline = 3,
  -- statuls line 显示
  laststatus = 0,
  inccommand = "split",

  title = true,

  -- 不显示显示当前的模式，交给插件 lualine.nvim 了
  showmode = false,

  -- 禁止创建备份文件
  backup = false,
  writebackup = false,
  swapfile = false,

  -- 搜索
  ignorecase = true,
  smartcase = true,
  -- 搜索高亮
  hlsearch = true,
  -- 边输入边搜索
  incsearch = true,

  -- 新的分割窗口在右边和下边出现
  splitright = true,
  splitbelow = true,
  splitkeep = "cursor",

  -- 当前行下划线标识
  cursorline = true,
  -- 当前行背景标识
  background = "dark",
  termguicolors = true,

  -- winblend = 0,
  confirm = true,

  -- 缩进
  autoindent = true, -- 自动缩进
  cindent = true, -- C/C++语法缩进
  tabstop = 2, -- tab 键宽度
  shiftwidth = 2, -- 缩进宽度
  softtabstop = -1, -- ofttabstop 的值为负数,会使用 shiftwidth 的值,两者保持一致,方便统一缩进.
  expandtab = true, -- 输入 Tab 字符时,自动替换成空
  shiftround = true,
  smartindent = true,

  sidescroll = 5,
  autochdir = true,

  -- scrolloff = 0, -- 预留屏幕上下的n行可见
  -- sidescrolloff = 0, -- 预留屏幕上下的n行可见
  ai = true, -- Auto indent
  si = true, -- Smart indent
  wrap = true, -- No wrap lines

  hidden = true,

  foldcolumn = "1", -- '0' is not bad
  -- https://stackoverflow.com/questions/8316139/how-to-set-the-default-to-unfolded-when-you-open-a-file
  foldlevel = 99, -- Using ufo provider need a large value, feel free to decrease the value
  foldenable = true,
  foldlevelstart = 99,
  -- za, zc， zo 会 折叠 {} 里的代码
  -- 开启 Folding
  -- vim.wo.foldmethod = "expr"
  -- vim.wo.foldexpr = "nvim_treesitter#foldexpr()"

  -- 表示每个项目目录下都可以存在 .nvim.lua 文件的配置信息
  exrc = true,
}

for key, value in pairs(options) do
  vim.opt[key] = value
end

-- CTRL + o：调整到上一次光标所在的行号上，即往后跳转
-- CTRL + i ：调整到jump list中当前记录的下一个记录的行号上，即往前调整

-- vim.cmd [[
--   set tagfunc=v:lua.vim.lsp.tagfunc
--   set jumpoptions+=stack
-- ]]
-- 或者
vim.o.jumpoptions = "stack"

-- 词 - 连接 作为一个词
vim.opt.iskeyword:append "-"
-- vim.cmd [[set formatoptions -= "c,r,o"]] -- 关闭换行自动注释
-- vim.opt.formatoptions:remove({ "c", "r", "o" })
vim.opt.shortmess:append { c = true } -- don't give |ins-completion-menu| messages

-- Finding files - Search down into subfolders
vim.opt.path:append "**"
vim.opt.wildignore:append {
  "*/node_modules/*",
  "*/target/*",
}

-- vim.opt.whichwrap = "b,s,<,>,[,],h,l"
-- vim.cmd "set whichwrap+=<,>,[,],h,l"
vim.opt.whichwrap:append { ["<"] = true, [">"] = true, [","] = true, h = true, l = true }

-- vim.opt.completeopt = { "menu", "menuone", "noselect", "noinsert" } -- must just for cmp
vim.opt.completeopt = { "menu", "menuone", "noselect" } -- mustly just for cmp

-- 撤销数据文件存储
vim.opt.undofile = true
-- presistent undo
vim.bo.undofile = true
-- opt.undodir = vim.fn.expand("$HOME/.local/share/nvim/undo")
vim.opt.undodir = vim.fn.expand "$HOME/.config/nvim/.tmp/undo"

-- 补全最多显示10行
vim.o.pumheight = 10
-- 命令行高，提供足够的显示空间
vim.o.cmdheight = 0

-- 显示一些可见字符
-- vim.o.list = true
-- vim.opt.backspace = "start,eol,indent"
-- vim.o.listchars = "space:·,tab:··,eol:↴"
-- vim.o.listchars = "tab:>~,trail:·,space:·,eol:↴"
-- vim.opt.listchars = "tab:»·,nbsp:+,trail:·,extends:→,precedes:←"
-- vim.o.listchars = "eol:⤶,space:·,tab:<=>,trail:"

-- 将制表符（tab）显示为…；将尾部空格（trail）显示为-；将左则超出屏幕范围部分（precedes）标识为«；将右侧超出屏幕范围部分（extends）标识为»。
-- space,tabs,newlines,trailing space,wrapped lines

vim.opt.list = true -- show whitespace
vim.opt.backspace = { "start", "eol", "indent" }
vim.opt.listchars:append "space: " -- "space:·"
vim.opt.listchars:append "eol: " -- "eol:↴"
vim.opt.listchars:append "tab:  " -- "tab:>~"

-- vim.opt.listchars:append "eol:⤶"
-- vim.opt.listchars:append "extends:→"
-- vim.opt.listchars:append "precedes:←"
-- vim.opt.listchars:append "nbsp:■"
-- tab:…»■,trail:■
-- vim.opt.listchars:append("space:·") -- "space:·"
-- vim.opt.listchars:append("eol: ")   -- "eol:↴"
-- vim.opt.listchars:append("tab:  ")  -- "tab:>~"
-- vim.opt.fillchars = {
--   stl = " ",
--   stlnc = "-",
--   msgsep = " ",
--   foldopen = "",
--   foldclose = "",
--   fold = " ",
--   foldsep = " ",
--   diff = "╱",
--   eob = " ",
-- }

-- 启用鼠标
vim.opt.mouse:append "a"
vim.opt.clipboard:append { "unnamedplus" }
vim.opt.clipboard:prepend { "unnamed", "unnamedplus" }

-- 编辑器左侧会多一列, 显示一些图标指示
vim.opt.signcolumn = "yes"
-- 右侧参考线，超过表示代码太长了，考虑换行
-- opt.colorcolumn = "80"
vim.opt.colorcolumn:append "80"
vim.opt.colorcolumn:append "100"
vim.opt.colorcolumn:append "120"
vim.opt.colorcolumn:append "140"
