-- 设置重新进入文件时，光标留在上次退出的地方, 这个可以替掉插件 ethanholz/nvim-lastplace
vim.cmd [[
 augroup back_to_leave
    autocmd!
    autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
  augroup END
]]

-- 插入模式绝对行号，其他模式相对行号
vim.cmd [[
  augroup toggle_relative_number
    autocmd!
    autocmd InsertEnter * :set norelativenumber
    autocmd InsertLeave * :set relativenumber
  augroup END
]]

-- 焦点消失的时候自动保存
vim.cmd [[
  augroup toggle_focuslost
    au!
    au FocusLost * :wa | echo ""
    au FocusGained,BufEnter * :checktime
  augroup END
]]
-- vim.api.nvim_create_autocmd({ "InsertLeave", "FocusLost" }, {
--   callback = function()
--     -- 事件采用了InsertLeave, 即当编辑器离开插入模式, 便触发保存, 通过Neovim的内置函数vim.fn.execute完成调用.

--     -- 并且, 在执行write之前调用了silent!不显示错误信息, 所以这里就需要注意, 如果保存了一个未命名的新文件, 并不会提示错误, 但是其实并没有完成保存.

--     -- 这里不加pattern参数的话默认是对全局生效, 注意这里我还加了一个提示弹窗, 通过notify插件来完成,
--     -- vim.fn.execute("silent! write")
--     vim.fn.execute("silent write")
--     vim.notify("Auto Saved!", vim.log.levels.INFO, {})
--   end,
-- })

-- 在 copy 后高亮一小会
-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup("YankHighlight", { clear = true })
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function()
    vim.highlight.on_yank {
      higroup = "Visual",
      timeout = 300,
    }
  end,
  group = highlight_group,
  desc = "Highlight on yank",
  pattern = "*",
})

vim.api.nvim_create_autocmd({ "FileType" }, {
  pattern = {
    "netrw",
    "Jaq",
    "qf",
    "git",
    "help",
    "man",
    "lspinfo",
    "oil",
    "spectre_panel",
    "lir",
    "DressingSelect",
    "tsplayground",
    "",
  },
  callback = function()
    vim.cmd [[
      nnoremap <silent> <buffer> q :close<CR>
      set nobuflisted
    ]]
  end,
})

vim.api.nvim_create_autocmd({ "CmdWinEnter" }, {
  callback = function()
    vim.cmd "quit"
  end,
})

vim.api.nvim_create_autocmd({ "CursorHold" }, {
  callback = function()
    local status_ok, luasnip = pcall(require, "luasnip")
    if not status_ok then
      return
    end
    if luasnip.expand_or_jumpable() then
      -- ask maintainer for option to make this silent
      -- luasnip.unlink_current()
      vim.cmd [[silent! lua require("luasnip").unlink_current()]]
    end
  end,
})

-- 关闭新行注释
vim.api.nvim_create_autocmd({ "BufEnter" }, {
  pattern = "*",
  callback = function()
    -- vim.opt.formatoptions = vim.opt.formatoptions - { "c", "r", "o" }
    vim.opt.formatoptions:remove { "c", "r", "o" }
  end,
})
-- vim.api.nvim_create_autocmd({ "FileType" }, {
--   pattern = { "*" },
--   command = "setlocal formatoptions-=c formatoptions-=r formatoptions-=o"
-- })

-- 没有工作,
-- vim.cmd [[
--   augroup illuminate_augroup
--     autocmd!
--     autocmd VimEnter * hi link illuminatedWord CursorLine
--   augroup END
-- ]]
-- vim.cmd [[
--   augroup illuminate_augroup
--     autocmd!
--     autocmd VimEnter * hi illuminatedWord cterm=underline gui=underline
--   augroup END
-- ]]

-- Runs a script that cleans out tex build files whenever I close out of a .tex file
vim.api.nvim_create_autocmd({ "VimLeave" }, {
  pattern = { "*.tex" },
  command = "!texclear %",
})

-- 指定的文件缩进
vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile", "FileType" }, {
  pattern = { "c", "cpp", "txt", "c.snippets", "cpp.snippets" },
  callback = function()
    vim.opt_local.tabstop = 4
    vim.opt_local.shiftwidth = 4
    vim.opt_local.softtabstop = 4
  end,
})
