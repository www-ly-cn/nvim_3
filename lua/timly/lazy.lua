-- vim.fn.stdpath("data") = ~/.local/share/nvim/
-- :lua print(vim.fn.stdpath("data"))
local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.notify "Installing plugin manager lazy.nvim ..."
  local out = vim.fn.system {
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  }
  if vim.v.shell_error ~= 0 then
    vim.api.nvim_echo({
      { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
      { out, "WarningMsg" },
      { "\nPress any key to exit..." },
    }, true, {})
    vim.fn.getchar()
    os.exit(1)
  end
  vim.notify "Plugin manager installed complete."
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
  { import = "timly.plugins" },
  { import = "timly.plugins.lsp" },
}, {
  checker = {
    enabled = true,
    notify = true,
  },
  change_detection = {
    notify = true,
  },
  install = {
    colorscheme = { "everforest", "tokyonight", "default" },
  },
  ui = {
    border = "rounded",
  },
  default = {
    lazy = true,
    version = true,
  },
  performance = {
    rtp = {
      -- disable some rtp plugins(禁用一些插件)
      disabled_plugins = {
        "gzip",
        -- "matchit",
        -- "matchparen",
        -- "netrwPlugin", -- vim 的原生文档树
        "tarPlugin",
        "tohtml",
        "tutor",
        "zipPlugin",
      },
      cache = {
        enabled = true,
        path = vim.fn.stdpath "cache" .. "lazy/cache",
        disable_events = {
          "VimEnter",
          "BufReadPre",
        },
        ttl = 3600 * 24 * 5,
      },
      reset_packpath = true, -- reset the package path to improve startup time
    },
  },
})
